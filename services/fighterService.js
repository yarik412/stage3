const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    add(data){
        const items = FighterRepository.create(data)
        if(!items){
            throw Error("Can't create this fighter!")
        }else{
            return items
        }
    }
    getAll(){
        const item = FighterRepository.getAll();
        if(!item) {
            throw Error("Fighter's not found");
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = FighterRepository.update(id,dataToUpdate)
        if(!item) {
            throw Error("Can't update this fighter!")
        }
        return item
    }

    delete(id) {
        const item = FighterRepository.delete(id)
        if(item.length===0) {
            throw Error("Can`t delete this fighter")
        }
        return item
    }


    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            throw Error("Fighter not found!")
        }
        return item;
    }
    // TODO: Implement methods to work with fighters
}

module.exports = new FighterService();