const { UserRepository } = require('../repositories/userRepository');

class UserService {

    getAll(){
        const item = UserRepository.getAll();
        if(!item) {
            throw Error('Users not found');
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = UserRepository.update(id,dataToUpdate)
        if(!item) {
            throw Error('User not found!')
        }
        return item
    }

    delete(id) {
        const item = UserRepository.delete(id)
        if(item.length===0) {
            throw Error('User not found!')
        }
        return item
    }

    add(user) {
        const item = UserRepository.create(user);
        if(!item) {
            throw Error("User entity to create is not valid");
        }
        return item;
    }

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw Error("User not found!")
        }
        return item;
    }
}

module.exports = new UserService();