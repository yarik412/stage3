const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const checkKeys = (reqFighter,origKeys)=>{
    Object.keys(reqFighter).forEach((key) => {
        if (origKeys[key]===undefined){
            throw Error(`It's not valid key: ${key}`)
        }
    })
}

const checkUniqueness = (reqFighter, key) => {
    const match = FighterService.getAll().filter((elem) => {
        return elem[key] === reqFighter[key]
    });
    if (match.length !== 0) {
        return false
    } else {
        return true
    }
}

const createFighterValid = (req, res, next) => {
    const newFighter = Object.assign({},fighter)
    const reqFighter = req.body
    try{
        checkKeys(reqFighter,fighter)
        if(reqFighter.id){
            res.status(400)
            throw Error("Json can't have id")
        }
        if(!reqFighter.name){
            res.status(400)
            throw Error('Name is empty');
        }else{
            if(!checkUniqueness(reqFighter,"name")){
                throw Error("Character with this name is already have!")
            }else{newFighter.name=reqFighter.name}
        }
        if(reqFighter.power){
            if(!isNaN(reqFighter.power)){
                if(!(0<reqFighter.power && reqFighter.power<100)){
                    res.status(400)
                    throw Error(`Power must be between 0 and 100!`);
                }else{newFighter.power=Number(reqFighter.power)}
            }else{
                res.status(400)
                throw Error('Power must be number!');
            }
        }else{
            res.status(400)
            throw Error('Power is empty');
        }
        if(reqFighter.defense){
            if(!isNaN(reqFighter.defense)){
                if(!(1<=reqFighter.defense && reqFighter.defense<=10)){
                    res.status(400)
                    throw Error(`Defence must be between 1 and 10!`);
                }else{newFighter.defense=Number(reqFighter.defense)}
            }else{
                res.status(400)
                throw Error('Defense must be number!');
            }
        }else{
            res.status(400)
            throw Error('Defense is empty');
        }
        res.data = newFighter
    }catch(err){
        res.status(400).err = err;
    }
    // TODO: Implement validatior for fighter entity during creation
    // next();
}

const updateFighterValid = (req, res, next) => {
    const reqFighter = req.body
    try{
        checkKeys(reqFighter,fighter)
        if(reqFighter.id){
            res.status(400)
            throw Error("Json can't have id")
        }
        if(reqFighter.name){
            if(!isNaN(reqFighter.name)){
                res.status(400)
                throw Error("Name must be a string, not a number")
            }
        }else{
            res.status(400)
            throw Error("Name can't be empty")
        }
        if(reqFighter.defense){
            if(!isNaN(reqFighter.defense)){
                if(!(1<=reqFighter.defense && reqFighter.defense<=10)){
                    throw Error(`Defence must be between 1 and 10!`);
                }
            }else{throw Error('Defense must be number!');}
        }else{
            res.status(400)
            throw Error("Defense can't be empty")
        }
        if(reqFighter.power){
            if(!isNaN(reqFighter.power)){
                if(!(0<reqFighter.power && reqFighter.power<100)){
                    throw Error(`Power must be between 0 and 100!`);
                }
            }
        }
        else{
            res.status(400)
            throw Error("Power can't be empty")
        }
    }catch(err){
        res.status(400).err = err;
    }
    // TODO: Implement validatior for fighter entity during update
    // next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;