const { user } = require('../models/user');
const UserService = require('../services/userService');

const EMAIL_RULE = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+(@gmail.com)*$/
const PHONE_RULE = /^\+380?([0-9]{9})$/

function validator(elem, rule) {
    if (rule.test(elem)) {
        return true
    } else {
        return false
    }
}

const checkKeys = (reqUser, origKeys) => {
    Object.keys(reqUser).forEach((key) => {
        if (origKeys[key] === undefined) {
            throw Error(`It's not valid key: ${key}`)
        }
    })
}
const checkUniqueness = (reqUser, key) => {
    const match = UserService.getAll().filter((elem) => {
        return elem[key] === reqUser[key]
    });
    if (match.length !== 0) {
        return false
    } else {
        return true
    }
}

const createUserValid = (req, res, next) => {
    const reqUser = req.body
    const newUser = Object.assign({}, user)
    try {
        checkKeys(reqUser, newUser);
        if (reqUser.id) {
            throw Error("Request can't have id")
        }
        if (!reqUser.firstName) {
            throw Error('First name is empty');
        } else { newUser.firstName = reqUser.firstName }
        if (!reqUser.lastName) {
            throw Error('Last name is empty');
        } else { newUser.lastName = reqUser.lastName }
        if (reqUser.email) {
            if (!validator(reqUser.email, EMAIL_RULE)) {
                throw Error(`Not valid ${reqUser.email}`);
            } else {
                if (!checkUniqueness(reqUser, 'email')) {
                    throw Error("This email already in use")
                } else { newUser.email = reqUser.email }
            }
        }else{
            throw Error("Email is empty")
        }
        if (reqUser.phoneNumber) {
            if (!validator(reqUser.phoneNumber, PHONE_RULE)) {
                throw Error(`Not valid ${reqUser.phoneNumber}`);
            } else {
                if (!checkUniqueness(reqUser, 'phoneNumber')) {
                    throw Error("This phone already in use")
                } else { newUser.phoneNumber = reqUser.phoneNumber }
            }
        }else{
            throw Error("Phone number is empty")
        }
        if (reqUser.password) {
            if (reqUser.password.length < 3) {
                throw Error(`Password length smaller than 3`);
            } else { delete newUser.password }
        } else {
            throw Error(`Password is empty!`);
        }
        res.data = newUser
    } catch (err) {
        res.status(400).err = err;
    }
    // TODO: Implement validatior for user entity during creation

    // next();
}

const updateUserValid = (req, res, next) => {
    const reqUser = req.body
    const newUser = Object.assign({}, user)
    try {
        UserService.search({id:req.params.id})
        checkKeys(reqUser, user);
        if (reqUser.id) {
            throw Error("Request can't have id")
        }
        if (reqUser.email) {
            if (!validator(reqUser.email, EMAIL_RULE)) {
                throw Error(`Not valid ${reqUser.email}`);
            } else{ newUser.email = reqUser.email }
        }else{
            throw Error("Email is empty")
        }
        if (reqUser.phoneNumber) {
            if (!validator(reqUser.phoneNumber, PHONE_RULE)) {
                throw Error(`Not valid ${reqUser.phoneNumber}`);
            } else {
                if (!checkUniqueness(reqUser, 'phoneNumber')) {
                    throw Error("This phone already in use")
                } else { newUser.phoneNumber = reqUser.phoneNumber }
            }
        }
        if (reqUser.password) {
            if (reqUser.password.length < 3) {
                throw Error(`Password length smaller than 3`);
            }
        }
        res.data = newUser
    } catch (err) {
        res.status(400).err = err;
    }
    // TODO: Implement validatior for user entity during update

    // next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;