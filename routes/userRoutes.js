const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();
router.post('/',(req,res,next)=>{
    try {
        createUserValid(req,res,next)
        if(res.data){
            const data = UserService.add(req.body)
            res.data=data;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware)

router.delete('/:id',(req,res,next)=>{
    try {
        const id = req.params.id
        res.data=UserService.delete(id)
    } catch (err) {
        res.status(404).err = err;
    }finally{
        next();
    }
},responseMiddleware)

router.get("/",(req,res,next)=>{
    try {
        res.data = UserService.getAll()
    } catch (err) {
        res.status(404).err = err;
    }finally{
        next();
    }
},responseMiddleware)

router.get("/:id",(req,res,next)=>{
    try {
        const id = {id:req.params.id}
        res.data = UserService.search(id)
    } catch (err) {
        res.status(404).err = err;
    }finally{
        next();
    }
},responseMiddleware)

router.put("/:id",(req,res,next)=>{
    try {
        updateUserValid(req,res,next)
        if(res.data){
            const id = req.params.id
            const dataToUpdate = req.body
            res.data = UserService.update(id,dataToUpdate)
        }
    } catch (err) {
        res.err = err;
    }finally{
        next();
    }
},responseMiddleware)

// TODO: Implement route controllers for user

module.exports = router;