const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();
router.post('/',(req,res,next)=>{
    try {
        createFighterValid(req,res,next)
        res.data = FighterService.add(req.body)
    } catch (err) {
        res.status(400).err = err;
    } finally {
        next();
    }
}, responseMiddleware)

router.delete('/:id',(req,res,next)=>{
    try {
        const id = req.params.id
        res.data = FighterService.delete(id)
    } catch (err) {
        res.status(404).err = err;
    }finally{
        next();
    }
},responseMiddleware)

router.get("/",(req,res,next)=>{
    try {
        res.data = FighterService.getAll()
    } catch (err) {
        res.status(404).err = err;
    }finally{
        next();
    }
},responseMiddleware)

router.get("/:id",(req,res,next)=>{
    try {
        const id = {id:req.params.id}
        res.data = FighterService.search(id)
    } catch (err) {
        res.status(404).err = err;
    }finally{
        next();
    }
},responseMiddleware)

router.put("/:id",(req,res,next)=>{
    try {
        updateFighterValid(req,res,next)
        const id = req.params.id
        const dataToUpdate = req.body
        res.data = FighterService.update(id,dataToUpdate)
    } catch (err) {
        res.err = err;
    }finally{
        next();
    }
},responseMiddleware)


// TODO: Implement route controllers for fighter

module.exports = router;