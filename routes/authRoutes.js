const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        const data = AuthService.login(req.body)
        const newdata = Object.assign({},data)
        delete newdata.password
        res.data = newdata;
        // TODO: Implement login action (get the user if it exist with entered credentials)
    } catch (err) {
        res.status(404).err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;